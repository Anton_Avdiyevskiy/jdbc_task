package com.epam.lab.view_controller;


import com.epam.lab.dto.Address;
import com.epam.lab.dto.Department;
import com.epam.lab.dto.Examination;
import com.epam.lab.dto.Student;
import com.epam.lab.dto.StudentPersonalData;
import com.epam.lab.meta_data.MetaData;
import com.epam.lab.service.AddressService;
import com.epam.lab.service.DepartmentService;
import com.epam.lab.service.ExaminationService;
import com.epam.lab.service.StudentPersonalDataService;
import com.epam.lab.service.StudentService;
import com.epam.lab.service_impl.AddressServiceImpl;
import com.epam.lab.service_impl.DepartmentServiceImpl;
import com.epam.lab.service_impl.ExaminationServiceImpl;
import com.epam.lab.service_impl.StudentPersonalDataServiceImpl;
import com.epam.lab.service_impl.StudentServiceImpl;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView {

  private Map<String, String> menu;
  private Map<String, PrintI> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private DepartmentService departmentService = new DepartmentServiceImpl();
  StudentService studentService = new StudentServiceImpl();
  AddressService addressService = new AddressServiceImpl();
  StudentPersonalDataService studentPersonalDataService = new StudentPersonalDataServiceImpl();
  ExaminationService examinationService = new ExaminationServiceImpl();
  MetaData metaData = new MetaData();


  public ConsoleView() {
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", " add new student-enter 1");
    menu.put("2", " find student by id-enter 2");
    menu.put("3", " find department by id-enter 3");
    menu.put("4", " find address by id-enter 4");
    menu.put("5", " find student data by id-enter 5");
    menu.put("6", " add new department-enter 6");
    menu.put("7"," delete student-enter 7");
    menu.put("8"," delete department and transfer student into another department-enter 8");
    menu.put("9"," get all meta data about database-enter 9");
    menu.put("10"," add new exam-enter 10");
    menu.put("11"," find exam by id-enter 11");
    menu.put("12"," find all exams-enter 12");
    menu.put("13"," set exam to student - enter 13");
    menu.put("14"," get all examination of some student by student`s id- enter 14");
    menu.put("Q", "   Q - exit");

    methodsMenu.put("1", this::addNewStudent);
    methodsMenu.put("2", this::findStudentById);
    methodsMenu.put("3", this::findDepartmentById);
    methodsMenu.put("4", this::findAddressById);
    methodsMenu.put("5", this::findStudentDataById);
    methodsMenu.put("6", this::addNewDepartment);
    methodsMenu.put("7", this::deleteStudent);
    methodsMenu.put("8", this::deleteDepartmentAndChangeDepForStudent);
    methodsMenu.put("9", this::getMetaDataOfDataBase);
    methodsMenu.put("10", this::addNewExam);
    methodsMenu.put("11", this::findExamById);
    methodsMenu.put("12", this::findAllExams);
    methodsMenu.put("13", this::setExaminationToStudent);
    methodsMenu.put("14", this::getExamsOfStudent);

  }
  protected  void addNewStudent(){
    System.out.println("enter name_surname:");
    String name = input.nextLine();
    System.out.println("enter father name_surname");
    String father_name = input.nextLine();
    System.out.println("enter sex of student:");
    String sex = input.nextLine();
    System.out.println("enter one of this departments");
    System.out.println(departmentService.findAllNamesOfDepartments());
    String nameOfDept = input.nextLine();
    int depId = departmentService.findIdByName(nameOfDept);
    Student student = new Student(name,father_name,sex,depId);
    int id = studentService.insertNewStudent(new Student(name,father_name,sex,depId));
    addPersonalData(id);
  }
  protected  void findStudentById(){
    System.out.println("enter id of student:");
    int id = input.nextInt();
    Student student= studentService.findStudentById(id);
    if(student==null){
      System.out.println("student with such id don`t exist");
    }else{
      System.out.println(student);
    }
  }
  protected  void findAddressById(){
    System.out.println("enter id of student address:");
    int id = input.nextInt();
    Address address = addressService.findAddressById(id);
    if(address==null){
      System.out.println("address with such id don`t exist");
    }else{
      System.out.println(address);
    }
  }
  protected  void findDepartmentById(){
    System.out.println("enter id of department:");
    int id = input.nextInt();
    Department department = departmentService.findDepartmentById(id);
    if(department==null){
      System.out.println("department with such id don`t exist");
    }else{
      System.out.println(department);
    }
  }
  protected  void findStudentDataById(){
    System.out.println("enter id of student data:");
    int id = input.nextInt();
    StudentPersonalData studentPersonalData = studentPersonalDataService.findById(id);
    if(studentPersonalData==null){
      System.out.println("student with such id don`t exist");
    }else{
      System.out.println(studentPersonalData);
    }
  }
  protected  void addNewDepartment(){
    System.out.println("enter name of department:");
    String name = input.nextLine();
    System.out.println("enter description");
    String description = input.nextLine();
    departmentService.addNewDepartment(new Department(name,description));
  }
  public  void addPersonalData(int id){
    Date dateJava = null;
    System.out.println("enter date of birth:");
    System.out.println("Enter check-in date (dd-mm-yy):");
    SimpleDateFormat myFormat = new SimpleDateFormat("dd-mm-yy");
    String dateOfBirth = input.nextLine();
    if(null != dateOfBirth && dateOfBirth.trim().length() > 0){
      try {
         dateJava = myFormat.parse(dateOfBirth);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    System.out.println("enter telephone");
    String telephone = input.nextLine();
    System.out.println("enter passport number");
    int passportNumber = input.nextInt();
    studentPersonalDataService.insertPersonalData(new StudentPersonalData(id,new java.sql.Date(dateJava.getTime()),
        telephone,passportNumber));
    addAddress(id);
  }
  public  void addAddress(int id){
    System.out.println("enter country:");
    String country = input.next();
    System.out.println("enter region");
    String region = input.next();
    System.out.println("enter street");
    String street = input.nextLine();
    addressService.addNewAddress(new Address(id,country,region,street));
  }
  public  void deleteAddress(int id){

    addressService.deleteAddress(id);
  }
  public  void deletePersonalData(int id){

    studentPersonalDataService.deletePersonalData(id);
  }
  public  void deleteStudent(){
    System.out.println("address and personal data of student will be deleted too");
    System.out.println("enter id of student,should be deleted");
    int id = input.nextInt();
    deleteAddress(id);
    deletePersonalData(id);
    studentService.deleteStudent(id);

  }
  public void deleteDepartmentAndChangeDepForStudent(){
    System.out.println("change what department should be deleted");
    System.out.println(departmentService.findAllNamesOfDepartments());
    String nameOfDeletedDep = input.next();
    int idOfDeletedDep = departmentService.findIdByName(nameOfDeletedDep);
    System.out.println(" in which department students should be transferred to");
    String nameOfNewDep = input.next();
    int idOfNewDep = departmentService.findIdByName(nameOfNewDep);
    departmentService.deleteDepAndChangeForStudents(idOfNewDep,idOfDeletedDep);
  }
  public void updateStudent(){
    System.out.println("enter id of student");
    int id = input.nextInt();
    System.out.println("enter new name");
    String name = input.next();
    studentService.updateStudentName(id,name);
  }
  public void getMetaDataOfDataBase() throws SQLException {
    metaData.getMetaData();
  }
  public void addNewExam(){
    System.out.println("enter id of exam");
    int id = input.nextInt();
    Date dateOfExamJava = null;
    System.out.println("enter date of exam:");
    System.out.println("Enter check-in date (dd-mm-yy):");
    SimpleDateFormat myFormat = new SimpleDateFormat("dd-mm-yy");
    String dateOfExam = input.next();
    if(null != dateOfExam && dateOfExam.trim().length() > 0){
      try {
        dateOfExamJava = myFormat.parse(dateOfExam);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    System.out.println("enter exam result");
    int examMark = input.nextInt();
    examinationService.insert(new Examination(id,new java.sql.Date(dateOfExamJava.getTime()),
        examMark));
  }
  public void findExamById(){
    System.out.println("enter id of exam");
    int id = input.nextInt();
    System.out.println(examinationService.findById(id));
  }
  public void findAllExams(){
    examinationService.findAll().forEach(System.out::println);
  }

  public void setExaminationToStudent(){
    System.out.println("enter student id");
    int studentId = input.nextInt();
    System.out.println("enter exam id");
    int examId = input.nextInt();
    examinationService.setExamToStudent(studentId,examId);
  }
  public void getExamsOfStudent(){
    System.out.println("enter student id,");
    int id = input.nextInt();
    examinationService.getDateExamsOfStudent(id).stream().map(e->e.getDateOfExam()).forEach(System.out::println);
  }
  private void printMenu() {
    System.out.println("MENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      printMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.next().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }

}
