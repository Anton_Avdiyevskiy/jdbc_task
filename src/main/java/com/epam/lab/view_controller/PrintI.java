package com.epam.lab.view_controller;

import java.sql.SQLException;

@FunctionalInterface
public interface PrintI {

  void print() throws SQLException;
}
