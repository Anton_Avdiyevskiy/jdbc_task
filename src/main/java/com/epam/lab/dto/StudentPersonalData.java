package com.epam.lab.dto;


import java.sql.Date;

public class StudentPersonalData {


  public static final String TABLE_NAME = "student_personal_data";
  public static final String ID_COLUMN = "id";
  public static final String DATE_OF_BIRTH = "date_of_birth";
  public static final String TELEPHONE = "telephone";
  public static final String PASSPORT_NUMBER = "passport_number";
  private int id;
  private Date dateOfBirth;
  private String telephoneNumber;
  private int passportNumber;

  public StudentPersonalData(int id, Date dateOfBirth, String telephoneNumber, int passportNumber) {
    this.id = id;
    this.dateOfBirth = dateOfBirth;
    this.telephoneNumber = telephoneNumber;
    this.passportNumber = passportNumber;
  }

  public StudentPersonalData() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }


  public String getTelephoneNumber() {
    return telephoneNumber;
  }

  public void setTelephoneNumber(String telephoneNumber) {
    this.telephoneNumber = telephoneNumber;
  }

  public int getPassportNumber() {
    return passportNumber;
  }

  public void setPassportNumber(int passportNumber) {
    this.passportNumber = passportNumber;
  }

  @Override
  public String toString() {
    return "StudentPersonalData{" +
        "id=" + id +
        ", dateOfBirth=" + dateOfBirth +
        ", telephoneNumber='" + telephoneNumber + '\'' +
        ", passportNumber=" + passportNumber +
        '}';
  }
}
