package com.epam.lab.dto;

import com.epam.lab.transformer.Column;
import com.epam.lab.transformer.Table;
import java.sql.Date;

@Table
public class Examination {
  @Column(name = "id")
  private int id;
  @Column(name = "data_of_exam")
  private Date dateOfExam;
  @Column(name = "exam_mark")
  int examMark;

  public Examination(int id, Date dateOfExam, int examMark) {
    this.id = id;
    this.dateOfExam = dateOfExam;
    this.examMark = examMark;
  }

  public Examination() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getDateOfExam() {
    return dateOfExam;
  }

  public void setDateOfExam(Date dateOfExam) {
    this.dateOfExam = dateOfExam;
  }

  public int getExamMark() {
    return examMark;
  }

  public void setExamMark(int examMark) {
    this.examMark = examMark;
  }

  @Override
  public String toString() {
    return "Examination{" +
        "id=" + id +
        ", dateOfExam=" + dateOfExam +
        ", examMark=" + examMark +
        '}';
  }
}
