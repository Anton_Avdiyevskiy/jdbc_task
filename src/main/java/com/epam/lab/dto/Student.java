package com.epam.lab.dto;


public class Student {

    public static final String TABLE_NAME = "student";
    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name_surname";
    public static final String FATHER_NAME = "father_name_surname";
    public static final String SEX = "sex";
    public static final String DEPARTMENT_ID_COLUMN = "id_specialty";
    private int id;
    private String nameSurname;
    private String fatherNameSurname;
    private String sex;
    private int id_speciality;

    public Student( String nameSurname, String fatherNameSurname, String sex,
        int id_speciality) {
        this.nameSurname = nameSurname;
        this.fatherNameSurname = fatherNameSurname;
        this.sex = sex;
        this.id_speciality = id_speciality;
    }

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getFatherNameSurname() {
        return fatherNameSurname;
    }

    public void setFatherNameSurname(String fatherNameSurname) {
        this.fatherNameSurname = fatherNameSurname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getId_speciality() {
        return id_speciality;
    }

    public void setId_speciality(int id_speciality) {
        this.id_speciality = id_speciality;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", nameSurname='" + nameSurname + '\'' +
                ", fatherNameSurname='" + fatherNameSurname + '\'' +
                ", sex='" + sex + '\'' +
                ", id_speciality=" + id_speciality +
                '}';
    }
}
