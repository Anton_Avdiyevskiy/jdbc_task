package com.epam.lab.dto;


public class Address {

  public static final String TABLE_NAME = "address";
  public static final String ID_COLUMN = "id";
  public static final String COUNTRY = "country";
  public static final String REGION = "region";
  public static final String STREET = "street";
  private int id;
  private String country;
  private String region;
  private String street;

  public Address(int id, String country, String region, String street) {
    this.id = id;
    this.country = country;
    this.region = region;
    this.street = street;
  }

  public Address() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Override
  public String toString() {
    return "Address{" +
        "id=" + id +
        ", country='" + country + '\'' +
        ", region='" + region + '\'' +
        ", street='" + street + '\'' +
        '}';
  }
}
