package com.epam.lab.dto;


public class Department {
    public static final String TABLE_NAME = "department";
    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "specialty_name";
    public static final String DEPARTMENT_DESCRIPTION = "description";

    private int id;
    private String specialtyName;
    private String description;

    public Department(String specialtyName, String description) {
        this.specialtyName = specialtyName;
        this.description = description;
    }

    public Department() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Department{" +
            "id=" + id +
            ", specialtyName='" + specialtyName + '\'' +
            ", description='" + description + '\'' +
            '}';
    }
}
