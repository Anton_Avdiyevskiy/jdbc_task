package com.epam.lab.service_impl;


import com.epam.lab.dao.StudentPersonalDataDao;
import com.epam.lab.dao_impl.StudentPersonalDataImpl;
import com.epam.lab.dto.StudentPersonalData;
import com.epam.lab.service.StudentPersonalDataService;
import java.util.List;

public class StudentPersonalDataServiceImpl implements StudentPersonalDataService {

  StudentPersonalDataDao studentPersonalDataDao = new StudentPersonalDataImpl();


  @Override
  public List<StudentPersonalData> findPersonalData() {
    return studentPersonalDataDao.findAll();
  }

  @Override
  public StudentPersonalData findById(int id) {
    return studentPersonalDataDao.findById(id);
  }

  @Override
  public void insertPersonalData(StudentPersonalData studentPersonalData) {
    studentPersonalDataDao.insert(studentPersonalData);
  }

  @Override
  public void updateTelephoneNumber(int id, String telephone) {
     studentPersonalDataDao.update(id,telephone);
  }

  @Override
  public void deletePersonalData(int id) {
    studentPersonalDataDao.delete(id);
  }
}
