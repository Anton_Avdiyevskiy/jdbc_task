package com.epam.lab.service_impl;


import com.epam.lab.dao.StudentDao;
import com.epam.lab.dao_impl.StudentDaoImpl;
import com.epam.lab.dto.Student;
import com.epam.lab.service.StudentService;
import java.util.List;

public class StudentServiceImpl implements StudentService {
  StudentDao studentDao = new StudentDaoImpl();

  @Override
  public List<Student> findAllStudents() {
    return studentDao.findAll();
  }

  @Override
  public Student findStudentById(int id) {
    return studentDao.findById(id);
  }

  @Override
  public int insertNewStudent(Student student) {
    int id = studentDao.insert(student);
    return id;
  }

  @Override
  public void updateStudentName(int id, String name) {
    studentDao.update(id,name);
  }

  @Override
  public void deleteStudent(int id) {
    studentDao.delete(id);
  }


}


