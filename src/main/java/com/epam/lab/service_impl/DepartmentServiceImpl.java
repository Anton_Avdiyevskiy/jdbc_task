package com.epam.lab.service_impl;




import com.epam.lab.dao.DepartmentDao;
import com.epam.lab.dao_impl.DepartmentDaoImpl;
import com.epam.lab.dto.Department;
import com.epam.lab.service.DepartmentService;
import java.util.List;

public class DepartmentServiceImpl implements DepartmentService {

  DepartmentDao departmentDao = new DepartmentDaoImpl();
  @Override
  public List<Department> findAllDepartments() {
    return departmentDao.findAll();
  }

  @Override
  public Department findDepartmentById(int id) {
    return departmentDao.findById(id);
  }

  @Override
  public void addNewDepartment(Department department) {
    departmentDao.insert(department);
  }

  @Override
  public void updateDepartmentName(int id, String name) {
    departmentDao.update(id,name);
  }

  @Override
  public void deleteDepartment(int id) {
    departmentDao.delete(id);
  }

  @Override
  public int findIdByName(String name) {
    return departmentDao.findIdByName(name);
  }

  @Override
  public List<String> findAllNamesOfDepartments() {
    return departmentDao.findAllNamesOfDepartments();
  }

  @Override
  public void deleteDepAndChangeForStudents(int newDepId, int oldDepId) {
    departmentDao.deleteDepAndChangeForStudents(newDepId,oldDepId);
  }
}
