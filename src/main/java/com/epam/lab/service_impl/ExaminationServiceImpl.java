package com.epam.lab.service_impl;

import com.epam.lab.dao.ExaminationDao;
import com.epam.lab.dao_impl.ExaminationDaoImpl;
import com.epam.lab.dto.Examination;
import com.epam.lab.service.ExaminationService;
import java.sql.Date;
import java.util.List;

public class ExaminationServiceImpl implements ExaminationService {
  ExaminationDao examinationDao = new ExaminationDaoImpl();

  @Override
  public List<Examination> findAll() {
    return examinationDao.findAll();
  }

  @Override
  public Examination findById(int id) {
    return examinationDao.findById(id);
  }

  @Override
  public void insert(Examination examination) {
    examinationDao.insert(examination);
  }

  @Override
  public void update(int id, Date date) {
    examinationDao.update(id,date);
  }

  @Override
  public void delete(int id) {
    examinationDao.delete(id);
  }

  @Override
  public void setExamToStudent(int idStudent, int idExam) {
    examinationDao.setExamToStudent(idStudent,idExam);
  }

  @Override
  public List<Examination> getDateExamsOfStudent(int studentId) {
    return examinationDao.getDateExamsOfStudent(studentId);
  }
}
