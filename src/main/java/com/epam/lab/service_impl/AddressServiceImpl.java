package com.epam.lab.service_impl;


import com.epam.lab.dao.AddressDao;
import com.epam.lab.dao_impl.AddressDaoImpl;
import com.epam.lab.dto.Address;
import com.epam.lab.service.AddressService;
import java.util.List;

public class AddressServiceImpl implements AddressService {
  AddressDao addressDao = new AddressDaoImpl();

  @Override
  public List<Address> findAllAddresses() {
    return addressDao.findAll();
  }

  @Override
  public Address findAddressById(int id) {
    return addressDao.findById(id);
  }

  @Override
  public void addNewAddress(Address address) {
      addressDao.insert(address);
  }

  @Override
  public void updateAddressCountry(int id, String country) {
    addressDao.update(id,country);
  }

  @Override
  public void deleteAddress(int id) {
    addressDao.delete(id);
  }
}
