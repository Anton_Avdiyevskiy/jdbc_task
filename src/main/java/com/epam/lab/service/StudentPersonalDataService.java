package com.epam.lab.service;


import com.epam.lab.dto.StudentPersonalData;
import java.util.List;

public interface StudentPersonalDataService {

  public List<StudentPersonalData> findPersonalData();

  public StudentPersonalData findById(int id);

  public void insertPersonalData(StudentPersonalData studentPersonalData);

  public void updateTelephoneNumber(int id, String telephone);

  public void deletePersonalData(int id);
}
