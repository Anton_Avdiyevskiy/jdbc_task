package com.epam.lab.service;

import com.epam.lab.dto.Student;
import java.util.List;

public interface StudentService {

  public List<Student> findAllStudents();

  public Student findStudentById(int id);

  public int insertNewStudent(Student student);

  public void updateStudentName(int id, String name);

  public void deleteStudent(int id);
}
