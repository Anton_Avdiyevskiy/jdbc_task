package com.epam.lab.service;

import com.epam.lab.dto.Department;
import java.util.List;


public interface DepartmentService {

  public List<Department> findAllDepartments();

  public Department findDepartmentById(int id);

  public void addNewDepartment(Department department);

  public void updateDepartmentName(int id, String name);

  public void deleteDepartment(int id);

  public int findIdByName(String name);

  public List<String> findAllNamesOfDepartments();

  public void deleteDepAndChangeForStudents(int newDepId, int oldDepId);

}
