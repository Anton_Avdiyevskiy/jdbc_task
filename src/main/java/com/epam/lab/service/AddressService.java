package com.epam.lab.service;


import com.epam.lab.dto.Address;
import java.util.List;

public interface AddressService {

  public List<Address> findAllAddresses();

  public Address findAddressById(int id);

  public void addNewAddress(Address address);

  public void updateAddressCountry(int id, String country);

  public void deleteAddress(int id);

}
