package com.epam.lab.service;


import com.epam.lab.dto.Examination;
import java.sql.Date;
import java.util.List;

public interface ExaminationService {

  public List<Examination> findAll();

  public Examination findById(int id);

  public void insert(Examination examination);

  public void update(int id, Date date);

  public void delete(int id);

  public void setExamToStudent(int idStudent, int idExam);

  public List<Examination> getDateExamsOfStudent(int studentId);

}
