package com.epam.lab.dao_impl;


import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.StudentPersonalDataDao;
import com.epam.lab.dto.StudentPersonalData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StudentPersonalDataImpl implements StudentPersonalDataDao {
  public static final String SQL_FIND_ALL = "select * from " + StudentPersonalData.TABLE_NAME;
  public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + StudentPersonalData.ID_COLUMN + " = ?";
  public static final String SQL_INSERT = "insert into " + StudentPersonalData.TABLE_NAME + " ("+ StudentPersonalData.ID_COLUMN+
      ", " + StudentPersonalData.DATE_OF_BIRTH + ", " + StudentPersonalData.TELEPHONE + ", " + StudentPersonalData.PASSPORT_NUMBER+") values (?,?,?,?)";
  public static final String SQL_UPDATE = "update " + StudentPersonalData.TABLE_NAME + " set "
      + StudentPersonalData.TELEPHONE +" = ? "+ "  where " + StudentPersonalData.ID_COLUMN + " = ?";
  public static final String SQL_DELETE = "delete from " + StudentPersonalData.TABLE_NAME + " where " +
      StudentPersonalData.ID_COLUMN + " = ?";

  private DataSource dataSource = new DataSource("jdbc:mysql://localhost/univer", "root", "root");
  private static final Logger log = LogManager.getLogger(AddressDaoImpl.class);

  public List<StudentPersonalData> findAll() {
    List<StudentPersonalData> result = new ArrayList<StudentPersonalData>();
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        StudentPersonalData studentPersonalData = new StudentPersonalData();
        studentPersonalData.setId(rs.getInt(StudentPersonalData.ID_COLUMN));
        studentPersonalData.setDateOfBirth(rs.getDate(StudentPersonalData.DATE_OF_BIRTH));
        studentPersonalData.setPassportNumber(rs.getInt(StudentPersonalData.PASSPORT_NUMBER));
        studentPersonalData.setTelephoneNumber(rs.getString(StudentPersonalData.TELEPHONE));
        result.add(studentPersonalData);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return result;
  }

  public StudentPersonalData findById(int id) {
    StudentPersonalData studentPersonalData = null;
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
      statement.setInt(1, id);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        studentPersonalData = new StudentPersonalData();
        studentPersonalData.setId(rs.getInt(StudentPersonalData.ID_COLUMN));
        studentPersonalData.setTelephoneNumber(rs.getString(StudentPersonalData.TELEPHONE));
        studentPersonalData.setDateOfBirth(rs.getDate(StudentPersonalData.DATE_OF_BIRTH));
        studentPersonalData.setPassportNumber(rs.getInt(StudentPersonalData.PASSPORT_NUMBER));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return studentPersonalData;
  }

  public void insert(StudentPersonalData studentPersonalData) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
      statement.setInt(1, studentPersonalData.getId());
      statement.setDate(2, studentPersonalData.getDateOfBirth());
      statement.setString(3, studentPersonalData.getTelephoneNumber());
      statement.setInt(4, studentPersonalData.getPassportNumber());
      statement.execute();

    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void update(int id, String country) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
      statement.setString(1, country);
      statement.setInt(2, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void delete(int id) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
      statement.setInt(1, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

}
