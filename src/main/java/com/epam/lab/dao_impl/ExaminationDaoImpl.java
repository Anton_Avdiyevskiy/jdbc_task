package com.epam.lab.dao_impl;


import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.ExaminationDao;
import com.epam.lab.dto.Examination;
import com.epam.lab.transformer.ResultSetMapper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ExaminationDaoImpl implements ExaminationDao {

  public static final String SQL_FIND_ALL = "select * from examination";
  public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where id= ?";
  public static final String SQL_INSERT = "insert into  examination (id,data_of_exam,exam_mark) values (?,?,?)";
  public static final String SQL_UPDATE = "update examination set data_of_exam = ?   where  id = ?";
  public static final String SQL_DELETE = "delete from examination where id = ?";
  public static final String SQL_SET_EXAM_TO_STUDENT = "insert into  examination_student"
      + " (id_student,id_examination) values (?,?)";
  public static final String SQL_FIND_DATE_OF_EXAM_OF_SOME_STUDENT =
      "select data_of_exam from examination e "
          + "join examination_student es on e.id=es.id_examination join student s on s.id=es.id_student where s.id = ?";

  private DataSource dataSource = new DataSource("jdbc:mysql://localhost/univer", "root", "root");
  private static final Logger log = LogManager.getLogger(AddressDaoImpl.class);

  public List<Examination> findAll() {
    List<Examination> result = new ArrayList<>();
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
      ResultSet rs = statement.executeQuery();
      ResultSetMapper<Examination> resultSetMapper = new ResultSetMapper<Examination>();
      result = resultSetMapper.mapResultSetToObject(rs, Examination.class);
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return result;
  }

  public Examination findById(int id) {
    Examination examination = null;
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
      statement.setInt(1, id);
      ResultSet rs = statement.executeQuery();
      ResultSetMapper<Examination> resultSetMapper = new ResultSetMapper<Examination>();
      examination = resultSetMapper.mapResultSetToObject(rs, Examination.class)
          .stream()
          .filter(exam -> exam.getId() == id).findAny().get();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return examination;
  }

  public void insert(Examination examination) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
      statement.setInt(1, examination.getId());
      statement.setDate(2, examination.getDateOfExam());
      statement.setInt(3, examination.getExamMark());
      statement.execute();

    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void update(int id, Date date) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
      statement.setDate(1, date);
      statement.setInt(2, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void delete(int id) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
      statement.setInt(1, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  @Override
  public void setExamToStudent(int idStudent, int idExam) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_SET_EXAM_TO_STUDENT);
      statement.setInt(1, idStudent);
      statement.setInt(2, idExam);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }

  }

  @Override
  public List<Examination> getDateExamsOfStudent(int studentId) {
    Connection connection = null;
    List<Examination> list = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection
          .prepareStatement(SQL_FIND_DATE_OF_EXAM_OF_SOME_STUDENT);
      statement.setInt(1, studentId);
      ResultSet rs = statement.executeQuery();
      ResultSetMapper<Examination> resultSetMapper = new ResultSetMapper<Examination>();
      list = resultSetMapper.mapResultSetToObject(rs, Examination.class);

    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return list;
  }
}
