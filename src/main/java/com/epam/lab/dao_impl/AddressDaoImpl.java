package com.epam.lab.dao_impl;


import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.AddressDao;
import com.epam.lab.dto.Address;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AddressDaoImpl implements AddressDao {
  public static final String SQL_FIND_ALL = "select * from " + Address.TABLE_NAME;
  public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + Address.ID_COLUMN + " = ?";
  public static final String SQL_INSERT = "insert into " + Address.TABLE_NAME + " ("+ Address.ID_COLUMN+", " + Address.COUNTRY + ", " + Address.REGION + ", "+ Address.STREET+") values (?,?,?,?)";
  public static final String SQL_UPDATE = "update " + Address.TABLE_NAME + " set " + Address.COUNTRY +" = ? "+ "  where " + Address.ID_COLUMN + " = ?";
  public static final String SQL_DELETE = "delete from " + Address.TABLE_NAME + " where " + Address.ID_COLUMN + " = ?";

  private DataSource dataSource = new DataSource("jdbc:mysql://localhost/univer", "root", "root");
  private static final Logger log = LogManager.getLogger(
      AddressDaoImpl.class);

  public List<Address> findAll() {
    List<Address> result = new ArrayList<Address>();
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        Address address = new Address();
        address.setId(rs.getInt(Address.ID_COLUMN));
        address.setCountry(rs.getString(Address.COUNTRY));
        address.setRegion(rs.getString(Address.REGION));
        address.setStreet(rs.getString(Address.STREET));
        result.add(address);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return result;
  }

  public Address findById(int id) {
    Address address = null;
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
      statement.setInt(1, id);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        address = new Address();
        address.setId(rs.getInt(Address.ID_COLUMN));
        address.setCountry(rs.getString(Address.COUNTRY));
        address.setRegion(rs.getString(Address.REGION));
        address.setStreet(rs.getString(Address.STREET));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return address;
  }

  public void insert(Address address) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
      statement.setInt(1, address.getId());
      statement.setString(2, address.getCountry());
      statement.setString(3, address.getRegion());
      statement.setString(4, address.getStreet());
      statement.execute();

    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void update(int id, String country) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
      statement.setString(1, country);
      statement.setInt(2, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void delete(int id) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
      statement.setInt(1, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }
}
