package com.epam.lab.dao_impl;




import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.StudentDao;
import com.epam.lab.dto.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StudentDaoImpl implements StudentDao {

    public static final String SQL_FIND_ALL = "select * from " + Student.TABLE_NAME;
    public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + Student.ID_COLUMN + " = ?";
    public static final String SQL_INSERT = "insert into " + Student.TABLE_NAME + " ( "+ Student.ID_COLUMN+" , "  + Student.NAME_COLUMN+" , "+
        Student.FATHER_NAME+" , "+ Student.SEX  +" , "+ Student.DEPARTMENT_ID_COLUMN + ") values (?,?,?,?,?)";
    public static final String SQL_UPDATE = "update " + Student.TABLE_NAME + " set " + Student.NAME_COLUMN+" = ? " + " where " + Student.ID_COLUMN + " = ?";
    public static final String SQL_DELETE = "delete from " + Student.TABLE_NAME + " where " + Student.ID_COLUMN + " = ?";
    public static final String SQL_MAX_ID = "select max(id) as id from student";


    private DataSource dataSource = new DataSource("jdbc:mysql://localhost/univer", "root", "root");
    private static final Logger log = LogManager.getLogger(
        StudentDaoImpl.class);

    public List<Student> findAll() {
        List<Student> result = new ArrayList();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt(Student.ID_COLUMN));
                student.setNameSurname(rs.getString(Student.NAME_COLUMN));
                student.setFatherNameSurname(rs.getString(Student.FATHER_NAME));
                student.setSex(rs.getString(Student.SEX));
                student.setId_speciality(rs.getInt(Student.DEPARTMENT_ID_COLUMN));
                result.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("SQLException");
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("SQLException");
            }
        }
        return result;
    }

    public Student findById(int id) {
        Student student = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                student = new Student();
                student.setId(rs.getInt(Student.ID_COLUMN));
                student.setNameSurname(rs.getString(Student.NAME_COLUMN));
                student.setFatherNameSurname(rs.getString(Student.FATHER_NAME));
                student.setSex(rs.getString(Student.SEX));
                student.setId_speciality(rs.getInt(Student.DEPARTMENT_ID_COLUMN));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("SQLException");
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("SQLException");
            }
        }
        return student;
    }

    public int insert(Student student) {
        Connection connection = null;
        student.setId(getMaxId()+1);
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setInt(1, student.getId());
            statement.setString(2, student.getNameSurname());
            statement.setString(3, student.getFatherNameSurname());
            statement.setString(4, student.getSex());
            statement.setInt(5, student.getId_speciality());
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            log.error("SQLException");
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("SQLException");
            }
        }
        return student.getId();
    }

    public void update(int id, String name) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, name);
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("SQLException");
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("SQLException");
            }
        }
    }

    public void delete(int id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("SQLException");
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                log.error("SQLException");
            }
        }
    }
    public int getMaxId(){
        int maxId=0;
        try {
            try(Connection connection = dataSource.getConnection()){
                Statement statement = connection.createStatement();
                statement.execute(SQL_MAX_ID);
                ResultSet rs = statement.getResultSet();
                while (rs.next()) {
                     maxId = rs.getInt(Student.ID_COLUMN);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return maxId;
    }
}


