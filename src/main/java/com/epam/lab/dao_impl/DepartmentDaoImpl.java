package com.epam.lab.dao_impl;


import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.DepartmentDao;
import com.epam.lab.dto.Department;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DepartmentDaoImpl implements DepartmentDao {

  public static final String SQL_FIND_ALL = "select * from " + Department.TABLE_NAME;
  public static final String SQL_FIND_BY_ID =
      SQL_FIND_ALL + " where " + Department.ID_COLUMN + " = ?";
  public static final String SQL_INSERT =
      "insert into " + Department.TABLE_NAME + " (" + Department.ID_COLUMN + ", "
          + Department.NAME_COLUMN + ", " + Department.DEPARTMENT_DESCRIPTION + ") values (?,?,?)";
  public static final String SQL_UPDATE =
      "update " + Department.TABLE_NAME + " set " + Department.NAME_COLUMN + " = ? " + "  where "
          + Department.ID_COLUMN + " = ?";
  public static final String SQL_DELETE =
      "delete from " + Department.TABLE_NAME + " where " + Department.ID_COLUMN + " = ?";
  public static final String SQL_FIND_ID_BY_NAME = "select id from department where specialty_name like (?)";
  public static final String SQL_NAMES_OF_DEPTS = "select specialty_name from department";
  public static final String SQL_MAX_ID = "select max(id) as id from department";
  public static final String SQL_CHANGE_DEP_ON_DELETE = "update student set id_specialty = ? where  id_specialty = ?";
  private DataSource dataSource = new DataSource("jdbc:mysql://localhost/univer", "root", "root");
  private static final Logger log = LogManager.getLogger(
      DepartmentDaoImpl.class);

  public List<Department> findAll() {
    List<Department> result = new ArrayList<Department>();
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        Department department = new Department();
        department.setId(rs.getInt(Department.ID_COLUMN));
        department.setSpecialtyName(rs.getString(Department.NAME_COLUMN));
        department.setDescription(rs.getString(Department.DEPARTMENT_DESCRIPTION));
        result.add(department);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return result;
  }

  public Department findById(int id) {
    Department department = null;
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
      statement.setInt(1, id);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        department = new Department();
        department.setId(rs.getInt(Department.ID_COLUMN));
        department.setSpecialtyName(rs.getString(Department.NAME_COLUMN));
        department.setDescription(rs.getString(Department.DEPARTMENT_DESCRIPTION));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return department;
  }

  public void insert(Department department) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
      statement.setInt(1, getMaxId() + 1);
      statement.setString(2, department.getSpecialtyName());
      statement.setString(3, department.getDescription());
      statement.execute();

    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void update(int id, String name) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
      statement.setString(1, name);
      statement.setInt(2, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public void delete(int id) {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
      statement.setInt(1, id);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  @Override
  public int findIdByName(String name) {
    Connection connection = null;
    int id = 0;
    try {
      connection = dataSource.getConnection();
      PreparedStatement statement = connection.prepareStatement(SQL_FIND_ID_BY_NAME);
      statement.setString(1, name);
      ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        id = rs.getInt(Department.ID_COLUMN);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return id;
  }

  @Override
  public List<String> findAllNamesOfDepartments() {
    List<String> result = new ArrayList<>();
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      Statement statement = connection.createStatement();
      statement.execute(SQL_NAMES_OF_DEPTS);
      ResultSet rs = statement.getResultSet();
      while (rs.next()) {
        String name = rs.getString(Department.NAME_COLUMN);
        result.add(name);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
    return result;
  }

  @Override
  public void deleteDepAndChangeForStudents(int newDepId,int oldDepId){
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      connection.setAutoCommit(false);
      PreparedStatement statementUpdate = connection.prepareStatement(SQL_CHANGE_DEP_ON_DELETE);
      statementUpdate.setInt(1,newDepId);
      statementUpdate.setInt(2,oldDepId);
      statementUpdate.executeUpdate();
      PreparedStatement statementDelete = connection.prepareStatement(SQL_DELETE);
      statementDelete.setInt(1, oldDepId);
      statementDelete.execute();
      connection.commit();
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("SQLException");
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        log.error("SQLException");
      }
    }
  }

  public int getMaxId() {
    int maxId = 0;
    try {
      try (Connection connection = dataSource.getConnection()) {
        Statement statement = connection.createStatement();
        statement.execute(SQL_MAX_ID);
        ResultSet rs = statement.getResultSet();
        while (rs.next()) {
          maxId = rs.getInt(Department.ID_COLUMN);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return maxId;
  }
}
