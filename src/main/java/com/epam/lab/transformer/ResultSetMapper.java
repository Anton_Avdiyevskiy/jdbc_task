package com.epam.lab.transformer;


import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ResultSetMapper<T> {

  private static final Logger log = LogManager.getLogger(
      ResultSetMapper.class);


  private void setProperty(Object clazz, String fieldName, Object columnValue) {
    try {

      Field field = clazz.getClass().getDeclaredField(fieldName);
      field.setAccessible(true);
      field.set(clazz, columnValue);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  public List<T> mapResultSetToObject(ResultSet rs, Class clazz) {
    List<T> outputList = null;
    try {
      if (rs != null) {
        if (clazz.isAnnotationPresent(Table.class)) {
          ResultSetMetaData rsmd = rs.getMetaData();
          Field[] fields = clazz.getDeclaredFields();
          while (rs.next()) {
            T obj = (T) clazz.newInstance();
            for (int iterator = 0; iterator < rsmd.getColumnCount(); iterator++) {
              String columnName = rsmd.getColumnName(iterator + 1);
              Object columnValue = rs.getObject(iterator + 1);
              for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class)) {
                  Column column = field.getAnnotation(Column.class);
                  if (column.name().equalsIgnoreCase(columnName)
                      && columnValue != null) {
                    this.setProperty(obj, field.getName(), columnValue);
                    break;
                  }
                }
              }
            }
            if (outputList == null) {
              outputList = new ArrayList<T>();
            }
            outputList.add(obj);
          }
        } else {
          log.info("no Table annotation");
        }
      } else {
        return null;
      }
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    }

    return outputList;
  }
}