package com.epam.lab.dao;


import com.epam.lab.dto.Address;
import java.util.List;

public interface AddressDao {

  public List<Address> findAll();

  public Address findById(int id);

  public void insert(Address address);

  public void update(int id, String country);

  public void delete(int id);
}
