package com.epam.lab.dao;


import com.epam.lab.dto.Department;
import java.util.List;

public interface DepartmentDao {


  public List<Department> findAll();

  public Department findById(int id);

  public void insert(Department department);

  public void update(int id, String nameDepartment);

  public void delete(int id);

  public int findIdByName(String name);

  public List<String> findAllNamesOfDepartments();

  //public int updateDepartmentsToStudent(int newDepId, int oldDeptId);

  public void deleteDepAndChangeForStudents(int newDepId, int oldDepId);
}
