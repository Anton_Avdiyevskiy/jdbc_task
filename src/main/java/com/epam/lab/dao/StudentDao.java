package com.epam.lab.dao;


import com.epam.lab.dto.Student;
import java.util.List;

public interface StudentDao {


  public List<Student> findAll();

  public Student findById(int id);

  public int insert(Student student);

  public void update(int id, String name);

  public void delete(int id);
}
