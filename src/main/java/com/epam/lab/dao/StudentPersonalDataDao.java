package com.epam.lab.dao;


import com.epam.lab.dto.StudentPersonalData;
import java.util.List;

public interface StudentPersonalDataDao {

  public List<StudentPersonalData> findAll();

  public StudentPersonalData findById(int id);

  public void insert(StudentPersonalData studentPersonalData);

  public void update(int id, String telephone);

  public void delete(int id);
}
