package com.epam.lab.meta_data;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MetaData {

  private static final Logger log = LogManager.getLogger(MetaData.class);

  public  void getMetaData() throws SQLException {
    Connection connection = null;
    Statement statement = null;
    connection = DriverManager.getConnection("jdbc:mysql://localhost/univer",
        "root", "root");
    try {
      DatabaseMetaData databaseMetaData = connection.getMetaData();
      ResultSet resultSet = databaseMetaData.getTables(null, null, null,
          new String[]{"Table"});
      System.out.println("TABLES:");
      while (resultSet.next()) {
        System.out.print("table: ");
        System.out.println(resultSet.getString("TABLE_NAME"));
        ResultSet columns = databaseMetaData
            .getColumns(null, null, resultSet.getString("TABLE_NAME"),
                null);
        System.out.println("Columns of " + resultSet.getString("TABLE_NAME"));
        ResultSet primaryKeys = databaseMetaData.getPrimaryKeys(null, null,
            resultSet.getString("TABLE_NAME"));
        while (columns.next()) {
          String name = columns.getString("COLUMN_NAME");
          System.out.println(name);
          System.out.println("--------------------");

        }
        while (primaryKeys.next()) {
          System.out.println(primaryKeys.getString("COLUMN_NAME") + "=" + primaryKeys.getString
              ("PK_NAME"));
        }
        ResultSet foreignKeys = databaseMetaData.getImportedKeys(null, null,
            resultSet.getString("TABLE_NAME"));
        System.out.println("foreign key:");
        while (foreignKeys.next()) {
          System.out.println(foreignKeys.getString("PKTABLE_NAME") + ":" +
              foreignKeys.getString("PKCOLUMN_NAME") + "=" + foreignKeys.getString("FKTABLE_NAME")
              + ":"
              + foreignKeys.getString("FKCOLUMN_NAME"));
        }
      }
      resultSet.close();
      connection.close();
    } catch (SQLException e) {
      connection.rollback();
      log.info("rollback ");
    } finally {
      if (connection != null) {
        connection.close();
      }
    }
  }
}
